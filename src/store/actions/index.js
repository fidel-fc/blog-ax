export const fetchPostList = () => {
    return async (dispatch) => {
        const response = await
            fetch(`http://www.mocky.io/v2/5be5e3fa2f000082000fc3f8`)
                .then(response => {
                    return response.json();
                })
                .then(json => json);
        dispatch({type:'FETCH_POSTLIST', payload: response})
    }
};
export const fetchAuthorList = () => {
    return async (dispatch) => {
        const response = await
            fetch(`http://www.mocky.io/v2/5be5e3ae2f00005b000fc3f6`)
                .then(response => {
                    return response.json();
                })
                .then(json => {
                    return json});

            dispatch({type:'FETCH_AUTHORLIST', payload: response})
    }
};

export const setFilter = (authorId) => {
    return{
        type: "SET_FILTER",
        payload: authorId
    }
};

export const setSortDate = (sort) => {
    return{
        type: "SET_FILTER_SORT",
        payload: sort
    }
};
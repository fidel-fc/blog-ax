import {combineReducers} from "redux";

const fetchPostlistReducer = (state = [], action) =>{
    if (action.type === 'FETCH_POSTLIST'){
        return action.payload
    }
    return state
};
const fetchUserlistReducer = (state = [], action) =>{
    if (action.type === 'FETCH_AUTHORLIST'){
        return action.payload
    }
    return state
};

const setfilterReducer = (state = null, action) => {
    if (action.type === 'SET_FILTER') {
        return action.payload;
    } else {
        return state
    }
};

const dateSortReducer = (state = null, action) => {
    if (action.type === 'SET_FILTER_SORT') {
        return action.payload;
    } else {
        return state
    }
};

export default combineReducers({
        postList: fetchPostlistReducer,
        authorList: fetchUserlistReducer,
        filter: setfilterReducer,
        dateSort: dateSortReducer
    }
)
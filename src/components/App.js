import React, {useEffect, useState} from "react";
import { useSelector, useDispatch } from 'react-redux';
import {fetchPostList, fetchAuthorList} from '../store/actions/index'
import PostList from "./PostList";
import AuthorList from "./AuthorList";
import sortArray from "../helpers/sortDate";
import PostTitleList from "./PostTitleList";
import {MainLayout} from "../styles/Layout";
import {MainTitle, Sidebar} from "../styles/Styles";
import {Item} from "../styles/Styles";
import {setSortDate} from "../store/actions/index";
import Hamburguer from "./Hamburguer";

const App = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.postList);
    const [menu, setMenu] = useState(false);

    useEffect(()=> {
        dispatch(fetchPostList());
        dispatch(fetchAuthorList())
    }, [dispatch]);

    const clickHandler =()=>{
        console.log('fired')
        setMenu(!menu)
    };

    return(
        <MainLayout>
            <Hamburguer open={menu} clickHandler={clickHandler}/>
            <MainTitle>Blog APP</MainTitle>
                <Sidebar open={menu}>
                    <AuthorList/>
                    <PostTitleList posts={sortArray(posts, 'recent').slice(0,3)}/>
                    <h2>Ordenar Posts</h2>
                    <Item onClick={() => dispatch(setSortDate('recent'))}> Mais Recentes</Item>
                    <Item onClick={() => dispatch(setSortDate('less-recent'))}> Mais Antigos</Item>
                </Sidebar>
            <PostList posts={posts}/>
        </MainLayout>
    )
};

export default App;
import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {setFilter} from "../store/actions";
import {Item} from "../styles/Styles";

const AuthorList = () => {
    const authors = useSelector(state => state.authorList);
    const filter = useSelector(state => state.filter);
    const dispatch = useDispatch();

    const list = authors.map((author) => {
        return (
            <Item active={filter === author.id} onClick={() => dispatch(setFilter(author.id))} key={author.id}>
                {author.name}
            </Item>
        )
    });

    return(
        <div>
            <h2>Autores</h2>
            <Item active={!filter}  onClick={() => dispatch(setFilter(null))} >Todos os Posts</Item>
            <div>{list}</div>
        </div>
    )
};

export default AuthorList
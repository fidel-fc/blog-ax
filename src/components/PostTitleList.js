import React, {useEffect, useState} from "react";
import {Item} from "../styles/Styles";
import PropTypes from "prop-types";

const PostTitleList = ({posts}) => {
    const [postList, setPostList] = useState([]);

    useEffect(() => {
        setPostList(posts);
    }, [posts]);

    const list = postList.map((post, index) => {
        return (
            <Item key={index}>
                {post.title}
            </Item>
        )
    });

    return (
        <div>
            <h2>Posts recentes</h2>
            {list}
        </div>
    )
};

PostTitleList.propTypes = {
    posts: PropTypes.array
};

export default PostTitleList;
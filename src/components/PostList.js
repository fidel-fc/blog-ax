import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import moment from "moment";
import 'moment/locale/pt-br';
import sortArray from "../helpers/sortDate";
import findAuthor from "../helpers/findAuthor";
import {Articles} from "../styles/Layout";
import {PostCard} from "../styles/Styles";
import PropTypes from "prop-types";

const PostList = ({posts}) => {
    const authors = useSelector(state => state.authorList);
    const filter = useSelector(state => state.filter);
    const dateSort = useSelector(state=> state.dateSort);
    const [postList, setPostList] = useState([]);

    useEffect(() => {
        setPostList(posts);
    }, [posts]);

    useEffect(() =>{
        if (filter){
            setPostList(posts.filter(post => post.metadata.authorId === filter))
        }  else {
            setPostList(posts);
        }
    },  [filter]);

    useEffect(() =>{
        if (dateSort){
            setPostList(sortArray(postList, dateSort))
        }  else {
            setPostList(posts);
        }
    },  [dateSort]);

    const list = postList.map((post, index) => {
        return (
            <PostCard key={index}>
                <h1>{post.title}</h1>
                    <div>
                        <p>{post.body}</p>
                        <h4>{findAuthor(authors, post.metadata.authorId)} {moment(post.metadata.publishedAt).format('DD/MM/YY HH:MM')}</h4>
                    </div>
            </PostCard>
        )
    });

    return(
        <Articles>
            <div>
                {list}
            </div>
        </Articles>
    )
};

PostList.propTypes = {
    posts: PropTypes.array
};

export default PostList;
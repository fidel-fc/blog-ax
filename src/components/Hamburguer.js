import React from "react";
import {HamburguerMenu} from "../styles/Styles";
import PropTypes from 'prop-types';

export const Hamburguer = (props) =>{

    return (
        <HamburguerMenu open={props.open} onClick={() => props.clickHandler()}>
            <div className="icon"/>
        </HamburguerMenu>
    )
};

Hamburguer.propTypes = {
    open: PropTypes.bool,
    clickHandler: PropTypes.func
};

export default Hamburguer;
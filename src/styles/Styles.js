import styled from "styled-components";
import backgroundImage from "./img/background.jpg";

export const MainTitle = styled.h1`
background-image:  linear-gradient(rgba(0,0,0,0.90), rgba(0,0,0,0.90)), url(${backgroundImage});
background-size: cover ;
color:#ebebeb;
margin: 0;
padding: 40px;
display: flex;
align-items: center;
justify-content: center;
height: 100%;
grid-column: 1/3;
font-size: 30px;
font-weight: bold;
text-align: center;
`;

export const Sidebar = styled.aside`
height: 100vh;
min-height: auto;
overflow: auto;
position: sticky;
top: 0;
color:white;
display: flex;
flex-direction: column;
background-color:#1e1f26;

  h2{
  font-size: 1.2rem;
  font-weight: bold;
  color:#eb255c;
  padding: 2px 10px;
  text-transform: uppercase;
  }
  
  
@media (max-width: 756px) {
padding-top: 60px ;
width: 250px;
position: fixed;
overflow: hidden;
height: 100vh;
top: 0;
left: 0;
opacity: ${props => props.open ? '1' : '0'} ;
margin-left: ${props => props.open ? '0' : '-250px'};
min-height: auto;
grid-column: 1/3;
grid-row-start: 2;
background-color:rgba(30,31,38,0.95);
transition: all 0.2s;

  }
`;

export const Item = styled.div`
  font-size: 1rem;
  margin: 1px 0;
  cursor: pointer;
  padding: 5px 10px;  
  color: ${props => props.active ? `#ff6406` : "white"};
  user-select: none;
  
  
  
  &:hover{
  background-color:#111111;
  color:#ff6406;
  }
`;

export const PostCard = styled.div`
  padding: 30px;
  margin: 20px 10px;
  display: flex;
  flex-direction: column;
  border: 1px transparent solid;
  border-radius: 5px;
  background-color:#404040;
  
  h4{
  font-style:italic;
  }
`;

export const HamburguerMenu = styled.div`
display: none;

@media (max-width: 756px){
    position: fixed;
    height: 50px;
    width: 20px;
    top: 15px;
    left: 15px;
    z-index: 9001;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;

   .icon{
    width: 20px;
    height: ${props => props.open ? '6px' : '2px'} ;
    display: flex;
    background-color:#fff;
    position: relative;
    transition: all 0.5s;
  
        &:before{
        content: ' ';
        background-color:#fff;
        display: block;
        width: 20px;
        height:${props => props.open ? '0' : '2px'};
        position: absolute;
        top: 6px;
        transition: all 0.5s;
        }
        
        &:after{
        display: block;
        background-color:#fff;
        content: ' ';
        width: 20px;
        height:${props => props.open ? '0' : '2px'};
        position: absolute;
        bottom: 6px;
        transition: all 0.5s;
        }
   }
   }
   
`;



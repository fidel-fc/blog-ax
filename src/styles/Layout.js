import styled from "styled-components";

export const MainLayout = styled.div`
display: grid;
place-content: center;
grid-template-columns: 1fr 4fr;
grid-column-gap: 20px;
grid-template-rows: 30vh;
grid-auto-rows: auto;
width: 100%;
height: 100%;
position: relative;

@media (max-width: 756px) {
grid-column-gap: 0;

  }
`;

export const Articles = styled.div`
grid-column: 2/3;
grid-row-start: 2;

@media (max-width: 756px) {
grid-column: 1/3;
grid-row-start: 3;
  }
`;
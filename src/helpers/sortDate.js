const sortDate = (a , b) =>{
    return a.metadata.publishedAt > b.metadata.publishedAt ? -1 : a.metadata.publishedAt<b.metadata.publishedAt ? 1 : 0;
};
const sortDateDecrescent = (a , b) =>{
    return a.metadata.publishedAt < b.metadata.publishedAt ? -1 : a.metadata.publishedAt>b.metadata.publishedAt ? 1 : 0;
};

const sortArray = (array, mode) => {
    if (mode === 'recent') return [...array].sort(sortDate);
    else return [...array].sort(sortDateDecrescent);
};

export default sortArray;
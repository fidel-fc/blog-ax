const findAuthor = (authors, id) => {
    const authorName = authors.find(author => author.id === id);
    if (authorName) return authorName.name;
};

export default  findAuthor;
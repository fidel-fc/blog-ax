import findAuthor from "../helpers/findAuthor";

const authors =[
    {
        "name": "Jemma Chadwick",
        "id": 1
    },
    {
        "name": "Nicholas Jordan",
        "id": 2
    },
    {
        "name": "Zinnia Rickard",
        "id": 3
    },
    {
        "name": "Sophie Hawk",
        "id": 4
    }
];

test('find Author', () => {
    expect(findAuthor(authors, 3)).toBe('Zinnia Rickard');
    expect(findAuthor(authors, 1)).toBe('Jemma Chadwick');
    expect(findAuthor(authors, 1)).not.toBe('Zinnia Rickard');
});
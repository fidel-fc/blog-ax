Algumas considerações sobre o desafio:

- Pelo que entendi no desafio, não era para mostrar uma tema com os detalhes da publicação individualmente, porém a
feature pode ser facilmente adicionada, com adição das Rotas.
- Na lista de sumário das publicações recentes, escolhi mostrar apenas 3, mas o número pode ser facilmente alterado.

To-Do:
-Página individual de cada post.
-Loader durante o fetch.


-Para instalar as dependências:
npm install

=Para iniciar o projeto:
npm start

=Para rodar o modo de produção:
npm run build
